//============================================================================
// Name        : proyecto_final.cpp
// Author      :
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <windows.h>
#include <conio.h>
#include <string.h>
#include <string>
#include <dos.h>
#include <ctype.h>
#include <iomanip>
#include "funciones.h"
#include <iostream>
#include <vector>
#include <string>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <algorithm>
#include <sstream>
using namespace std;
void cabecera_compra(string palabra,string palabra2,string palabra3,string palabra4,string palabra5, int espacios);
void cabecera_usuario(string palabra,string palabra2,string palabra3,string palabra4,string palabra5,string palabra6,string palabra7, int espacios);
void cabecera_admin(string palabra,string palabra2,string palabra3,string palabra4,string palabra5,string palabra6,string palabra7,
					string palabra8,string palabra9,string palabra10,string palabra11,int espacios);
void convierte_a_mayuscula (string &palabra);
vector <string> recoge_datos (string linea);

struct medicina_admin
	{int codigo;
	string nombre;
	string concentracion;
	string nom_formal;
	string nom_simpl;
	string presentacion;
	int fracciones;
	string fecha;
	string reg_san;
	string laboratorio;
	float precio;
	void inicializar (vector <string> datos)
		{vector<string>::iterator v= datos.begin();
		codigo=atoi((*v).c_str());
		v++;
		nombre=(*v);
		v++;
		concentracion=(*v);
		v++;
		nom_formal=(*v);
		v++;
		nom_simpl=(*v);
		v++;
		presentacion=(*v);
		v++;
		fracciones=atoi((*v).c_str());
		v++;
		fecha=(*v);
		v++;
		reg_san=(*v);
		v++;
		laboratorio=(*v);
		v++;
		precio=atof((*v).c_str());
		}
	void imprimir(int espacios)
		{
		cout.setf(ios::left);
		cout.width(espacios);cout<<codigo;
		cout.width(espacios);cout<<nombre;
		cout.width(espacios);cout<<concentracion;
		cout.width(espacios);cout<<nom_formal;
		cout.width(espacios);cout<<nom_simpl;
		cout.width(espacios);cout<<presentacion;
		cout.width(espacios);cout<<fracciones;
		cout.width(espacios);cout<<fecha;
		cout.width(espacios);cout<<reg_san;
		cout.width(espacios);cout<<laboratorio;
		cout.width(espacios);cout<<precio<<endl;
		}
	};
struct medicina_usuario
		{
			float codigo;
			string nombre;
			string nom_simpl;
			string presentacion;
			int fracciones;
			string laboratorio;
			float precio;
			void inicializar (vector<string> a)
				{vector <string>::iterator v=a.begin();
				codigo=atof((*v).c_str());v++;
				nombre=*v;v++;
				nom_simpl=*v;v++;
				presentacion=*v;v++;
				fracciones=atoi((*v).c_str());v++;
				laboratorio=*v;v++;
				precio=atof((*v).c_str());
				}
			void imprimir(int espacios)
				{cout.setf(ios::left);
				cout.width(espacios);cout<<codigo;
				cout.width(espacios);cout<<nombre;
				cout.width(espacios);cout<<nom_simpl;
				cout.width(espacios);cout<<presentacion;
				cout.width(espacios);cout<<fracciones;
				cout.width(espacios);cout<<laboratorio;
				cout.width(espacios);cout<<precio<<endl;}
	};
struct medicina_comprada//Para hacer carrito de compras
{
	int codigo;
	string nombre;
	float precio;
	int cantidad_comprada;
	float sub_total;
	void rellena (medicina_usuario a, int cantidad)//Cantidad comprada y medcina
		{codigo=a.codigo;
		nombre=a.nombre;
		precio=a.precio;
		cantidad_comprada=cantidad;
		sub_total=precio*cantidad_comprada;
		}
	void imprime (int espacios)
			{	cout.setf(ios::left);
				cout.width(espacios);
				cout<<codigo;
				cout.width(espacios);
				cout<<nombre;
				cout.width(espacios);
				cout<<precio;
				cout.width(espacios);
				cout<<cantidad_comprada;
				cout.width(espacios);
				cout<<sub_total;
				cout<<endl;
			}

	string escribeArchivo(char delim)//Para hacer el historial
					{stringstream aux;
					aux<<codigo<<delim<<nombre<<delim
					<<precio<<delim<<cantidad_comprada<<endl;
					return aux.str();}
};
vector <string> recoge_datos (string linea)
				{vector <string> datos;
				string dato;
				stringstream l (linea);
				while (getline(l,linea,';'))
					{
						dato=linea;
						datos.push_back(dato);
					}
				return datos;
				}
vector <medicina_admin> hacer_lista_admin (string filename)
	{ifstream file (filename.c_str(),ios::in);
	string linea;
	vector<medicina_admin> medicamentos;
	medicina_admin med;
	while (getline(file,linea))
			{
				med.inicializar(recoge_datos(linea));
				medicamentos.push_back(med);
			}
	return medicamentos;
		}
vector <medicina_usuario> hacer_lista_usuario (string filename)
	{ifstream file (filename.c_str(),ios::in);
	string linea;
	vector<medicina_usuario> medicamentos;
	medicina_usuario med;
	while (getline(file,linea))
			{
				med.inicializar(recoge_datos(linea));
				medicamentos.push_back(med);
			}
	return medicamentos;
		}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Reportes
void reporte_para_admin (vector <medicina_admin> lista, int espacios)
	{
	cabecera_admin("Codigo","Nombre","Concentracion","Nombre formal","Tipo de medicamento","Presentacion","Fracciones","Fecha","Registro Sanitario","Laboratorio","Precio",35);
	vector <medicina_admin>::iterator v= lista.begin();
	while (v!=lista.end())
		{(*v).imprimir(espacios);
		v++;
		}
	}
void reporte_para_usuario(vector <medicina_usuario> lista, int espacios)
	{int i=0;
	cabecera_usuario("Codigo","Nombre","Tipo de medicamento","Presentacion","Fracciones","Laboratorio","Precio",35);
	vector <medicina_usuario>::iterator v= lista.begin();
	while (v!=lista.end())
		{i++;(*v).imprimir(espacios);
		if (i%50==0){cin.ignore();cin.get();}
		v++;
		}
	}
void reporte_para_usuario_compra(vector <medicina_comprada> lista, int espacios)
	{
	cabecera_compra("Codigo","Nombre","Precio Unitario","Cantidad Comprada","Subtotal",35);
	vector <medicina_comprada>::iterator v= lista.begin();
	while (v!=lista.end())
		{(*v).imprime(espacios);
		v++;
		}
	}
//////////////////////////////////////////////////////////////////////////////////////////////
//Variables globales
vector <medicina_admin> LISTA_ADMIN=hacer_lista_admin("datos/lista_admin.csv");
vector <medicina_usuario> LISTA_USUARIO=hacer_lista_usuario("datos/lista_usuario.csv");
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Objetos para ordenar
//Admin
bool nombre_desc_ad (const medicina_admin &a,const medicina_admin &b)
	{return (a.nombre<b.nombre);}
bool nombre_asc_ad (const medicina_admin &a,const medicina_admin &b)
	{return (a.nombre>b.nombre);}
bool tipo_de_medicamento_desc_ad(const medicina_admin &a,const medicina_admin &b)
	{return (a.nom_formal<b.nom_formal);}
bool tipo_de_medicamento_asc_ad(const medicina_admin &a,const medicina_admin &b)
	{return (a.nom_formal>b.nom_formal);}
bool codigo_desc_ad(const medicina_admin &a, const medicina_admin &b)
	{return (a.codigo<b.codigo);}
bool codigo_asc_ad(const medicina_admin &a, const medicina_admin &b)
	{return (a.codigo>b.codigo);}
//Usuario
bool nombre_desc_us (const medicina_usuario &a,const medicina_usuario &b)
	{return (a.nombre<b.nombre);}
bool nombre_asc_us (const medicina_usuario &a,const medicina_usuario &b)
	{return (a.nombre>b.nombre);}
bool tipo_de_medicamento_desc_us(const medicina_usuario &a,const medicina_usuario &b)
	{return (a.nom_simpl<b.nom_simpl);}
bool tipo_de_medicamento_asc_us(const medicina_usuario &a,const medicina_usuario &b)
	{return (a.nom_simpl>b.nom_simpl);}
bool codigo_desc_us(const medicina_usuario &a, const medicina_usuario &b)
	{return (a.codigo<b.codigo);}
bool codigo_asc_us(const medicina_usuario &a, const medicina_usuario &b)
	{return (a.codigo>b.codigo);}
bool precio_desc_us(const medicina_usuario &a, const medicina_usuario &b)
	{return (a.precio<b.precio);}
bool precio_asc_us(const medicina_usuario &a, const medicina_usuario &b)
	{return (a.precio>b.precio);}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Orden para admin
void ordena_lista_admin_nombre_desc (vector <medicina_admin> a)
	{sort(a.begin(),a.end(),nombre_desc_ad);}
void ordena_lista_admin_nombre_asc (vector <medicina_admin> a)
	{sort(a.begin(),a.end(),nombre_asc_ad);}
void ordena_lista_admin_codigo_desc (vector <medicina_admin> a)
	{sort(a.begin(),a.end(),codigo_desc_ad);}
void ordena_lista_admin_codigo_asc (vector <medicina_admin> a)
	{sort(a.begin(),a.end(),codigo_asc_ad);}
void ordena_lista_admin_tipo_desc(vector <medicina_admin> a)
	{sort(a.begin(),a.end(),tipo_de_medicamento_desc_ad);}
void ordena_lista_admin_tipo_asc(vector <medicina_admin> a)
	{sort(a.begin(),a.end(),tipo_de_medicamento_asc_ad);}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Orden para usuario
void ordena_lista_usuario_nombre_desc (vector <medicina_usuario> a)
	{sort(a.begin(),a.end(),nombre_desc_us);}
void ordena_lista_us_nombre_asc (vector <medicina_usuario> a)
	{sort(a.begin(),a.end(),nombre_asc_us);}
void ordena_lista_usuario_codigo_desc (vector <medicina_usuario> a)
	{sort(a.begin(),a.end(),codigo_desc_us);}
void ordena_lista_usuario_codigo_asc (vector <medicina_usuario> a)
	{sort(a.begin(),a.end(),codigo_asc_us);}
void ordena_lista_usuario_tipo_desc(vector <medicina_usuario> a)
	{sort(a.begin(),a.end(),tipo_de_medicamento_desc_us);}
void ordena_lista_usuario_tipo_asc(vector <medicina_usuario> a)
	{sort(a.begin(),a.end(),tipo_de_medicamento_asc_us);}
void ordena_lista_usuario_precio_desc(vector <medicina_usuario> a)
	{sort(a.begin(),a.end(),precio_desc_us);}
void ordena_lista_usuario_precio_asc(vector <medicina_usuario> a)
	{sort(a.begin(),a.end(),precio_asc_us);}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void ApellidosyNombres(string &linea);
medicina_usuario buscar_por_codigo (int codigo, vector <medicina_usuario> lista)
	{
	vector <medicina_usuario>::iterator v=lista.begin();
	while (v!=lista.end())
		{
			if (codigo==(*v).codigo)
			{break;}
			v++;
		}
	return (*v);
	}
medicina_admin buscar_por_codigo (int codigo, vector <medicina_admin> lista)
	{
	vector <medicina_admin>::iterator v=lista.begin();
	while (v!=lista.end())
		{
			if (codigo==(*v).codigo)
			{break;}
			v++;
		}
	return (*v);
	}
medicina_comprada buscar_por_codigo (int codigo, vector <medicina_comprada> lista)
	{
	vector <medicina_comprada>::iterator v=lista.begin();
	while (v!=lista.end())
		{
			if (codigo==(*v).codigo)
			{break;}
			v++;
		}
	return (*v);
	}
struct usuarios{
	string contra;
	string nombre;
	string apellidoP;
	string apellidoM;
	unsigned int DNI;
	unsigned int edad;
	void inicializar (vector <string> datos)
		{
			vector <string >::iterator v=datos.begin();
					contra=(*v);v++;
					nombre=(*v);v++;
					apellidoP=(*v);v++;
					apellidoM=(*v);v++;
					DNI=(atoi((*v).c_str()));v++;
					edad=(atoi((*v).c_str()));
		}
	string escribeArchivo(char delim)
		{
			stringstream file;
			file<<contra<<delim<<nombre<<delim<<apellidoP<<delim<<apellidoM<<delim<<DNI<<delim<<edad;
			return file.str();
		}
};
bool se_encuentra(int codigo,vector <medicina_usuario> lista)// Verifica que el codigo buscado existe
	{bool resp=false;
	vector <medicina_usuario>::iterator v=lista.begin();
	while (v!=lista.end())
		{
			if (codigo==(*v).codigo)
			{resp=true;break;}
			v++;
		}
	return resp;
	}

bool se_encuentra(string palabra,vector <medicina_usuario> lista)// Verifica que el codigo buscado existe
	{bool resp=false;
	convierte_a_mayuscula(palabra);
	vector <medicina_usuario>::iterator v=lista.begin();
	while (v!=lista.end())
		{
			if (palabra==(*v).nombre)
			{resp=true;break;}
			v++;
		}
	return resp;
	}
bool se_encuentra_tipo(string palabra,vector <medicina_usuario> lista)// Verifica que el codigo buscado existe
	{bool resp=false;
	convierte_a_mayuscula(palabra);
	vector <medicina_usuario>::iterator v=lista.begin();
	while (v!=lista.end())
		{
			if (palabra==(*v).nom_simpl)
			{resp=true;break;}
			v++;
		}
	return resp;
	}
bool se_encuentra(int codigo,vector <medicina_admin> lista)// Verifica que el codigo buscado existe
	{bool resp=false;
	vector <medicina_admin>::iterator v=lista.begin();
	while (v!=lista.end())
		{
			if (codigo==(*v).codigo)
			{resp=true;break;}
			v++;
		}
	return resp;
	}
bool se_encuentra(string palabra,vector <medicina_admin> lista)// Verifica que el codigo buscado existe
	{bool resp=false;
	convierte_a_mayuscula(palabra);
	vector <medicina_admin>::iterator v=lista.begin();
	while (v!=lista.end())
		{
			if (palabra==(*v).nombre)
			{resp=true;break;}
			v++;
		}
	return resp;
	}
bool se_encuentra_tipo(string palabra,vector <medicina_admin> lista)// Verifica que el codigo buscado existe
	{bool resp=false;
	convierte_a_mayuscula(palabra);
	vector <medicina_admin>::iterator v=lista.begin();
	while (v!=lista.end())
		{
			if (palabra==(*v).nom_simpl)
			{resp=true;break;}
			v++;
		}
	return resp;
	}
bool se_encuentra(int codigo,vector <medicina_comprada> lista)// Verifica que el codigo buscado existe
	{bool resp=false;
	vector <medicina_comprada>::iterator v=lista.begin();
	while (v!=lista.end())
		{
			if (codigo==(*v).codigo)
			{resp=true;break;}
			v++;
		}
	return resp;
	}

void quita_espacios(string &palabra) //le quita todos los espacios a la palabra al inicio y al final; ademas solo deja un espacio entre palabras
	{								// Por ejm: "  H   OLA  " -> "H OLA"
		while (palabra[0]==' ')
			{palabra.erase(ios::beg,1);}
	for (unsigned int i=0;i<palabra.size();i++)
		{
			if (palabra[i]==' ')
				{
					while (palabra[i+1]==' ')
						{palabra=palabra.erase(ios::beg+i+1,1);}
				}
		}

	for (unsigned int i=palabra.size()-1;palabra[i]==' ';i--)
		{palabra=palabra.erase(ios::beg+i,1);}
	}
void convierte_a_mayuscula (string &palabra)
	{quita_espacios(palabra);
	for (unsigned int i=0;i<palabra.size();i++)
		{if (isspace(palabra.c_str()[i]||isdigit(palabra.c_str()[i]))){continue;}//Se salta los espacios o numeros
		palabra[i]=toupper(palabra.c_str()[i]);}
	}
vector <medicina_admin> busqueda_por_nombre_admin(vector <medicina_admin> lista,string nombre)//Devuelve el medicamento en dif. presentaciones
	{vector <medicina_admin>::iterator v=lista.begin();
	medicina_admin encontrado;
	vector <medicina_admin> lista_de_encontrados;
	convierte_a_mayuscula(nombre);
	while (v!=lista.end())// Los medicamentos tienen su nombre en mayuscula
		{if ((*v).nombre==nombre)
		{encontrado=(*v);
		lista_de_encontrados.push_back(encontrado);}//encontro la medicina
		v++;}
	return lista_de_encontrados;
	}
vector <medicina_usuario> busqueda_por_nombre_usuario(vector <medicina_usuario> lista,string nombre)//Devuelve el medicamento en dif. presentaciones
	{vector <medicina_usuario>::iterator v=lista.begin();
	medicina_usuario encontrado;
	vector <medicina_usuario> lista_de_encontrados;
	convierte_a_mayuscula(nombre);
	while (v!=lista.end())// Los medicamentos tienen su nombre en mayuscula
		{if ((*v).nombre==nombre)
		{encontrado=(*v);
		lista_de_encontrados.push_back(encontrado);}//encontro la medicina
		v++;}
	return lista_de_encontrados;
	}
vector <medicina_admin> busqueda_por_tipo_admin(vector <medicina_admin> lista,string nombre)//Devuelve el medicamento en dif. presentaciones
	{vector <medicina_admin>::iterator v=lista.begin();
	medicina_admin encontrado;
	vector <medicina_admin> lista_de_encontrados;
	convierte_a_mayuscula(nombre);
	while (v!=lista.end())// Los medicamentos tienen su nombre en mayuscula
		{if ((*v).nom_simpl==nombre)
		{encontrado=(*v);
		lista_de_encontrados.push_back(encontrado);}//encontro la medicina
		v++;}
	return lista_de_encontrados;
	}
vector <medicina_usuario> busqueda_por_tipo_usuario(vector <medicina_usuario> lista,string nombre)//Devuelve el medicamento en dif. presentaciones
	{vector <medicina_usuario>::iterator v=lista.begin();
	medicina_usuario encontrado;
	vector <medicina_usuario> lista_de_encontrados;
	convierte_a_mayuscula(nombre);
	while (v!=lista.end())// Los medicamentos tienen su nombre en mayuscula
		{if ((*v).nom_simpl==nombre)
		{encontrado=(*v);
		lista_de_encontrados.push_back(encontrado);}//encontro la medicina
		v++;}
	return lista_de_encontrados;
	}
vector <medicina_usuario> busqueda_por_concentracion(vector <medicina_usuario> lista,int concentracion)//Devuelve el medicamento en dif. presentaciones
	{vector <medicina_usuario>::iterator v=lista.begin();
	medicina_usuario encontrado;
	vector <medicina_usuario> lista_de_encontrados;
	while (v!=lista.end())// Los medicamentos tienen su nombre en mayuscula
		{if ((*v).fracciones==concentracion)
		{encontrado=(*v);
		lista_de_encontrados.push_back(encontrado);}//encontro la medicina
		v++;}
	return lista_de_encontrados;
	}
void no_se_encuentra_codigo(int codigo)
	{cout<<"No se encuentra el codigo: "<<codigo<<" en la lista de medicamentos"<<endl;}
void no_se_encuentra_nombre(string nombre)
	{cout<<"No se encuentra el nombre: "<<nombre<<" en la lista de medicamentos"<<endl;}
void no_se_encuentra_tipo(string nombre)
	{cout<<"No se encuentra el tipo de medicamento: "<<nombre<<" en la lista de medicamentos"<<endl;}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void agrega_medicina_comprada(medicina_usuario a,int cantidad, vector <medicina_comprada> &b)
	{medicina_comprada c;
	c.rellena(a,cantidad);
	b.push_back(c);}
void elimina_medicina(vector <medicina_comprada>&c, int codigo)
	{vector<medicina_comprada>::iterator v= c.begin();
	while (v!=c.end())
		{
		if (codigo==(*v).codigo) {break;}
		v++;
		}
	c.erase(ios::beg+v);
	}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Menu
void menu_busqueda_usuario(vector <medicina_usuario> b,fstream &file2);
void menu_post_compra(vector <medicina_comprada>&b,fstream &file2);
void menu_busqueda_admin(vector <medicina_admin> b);
void floro();
void ver_carrito (vector<medicina_comprada>&lista, fstream &file2)// Falta que vuelvan al menu principal
	{reporte_para_usuario_compra(lista,35);
	floro();
	menu_post_compra(lista,file2);
	}
void floro()
	{cout<<"Presione cualuier tecla para volver...";cin.get();cin.ignore();}
void interfaz_nombre(vector<medicina_usuario> a, fstream &file2)
		{string nombre;
		cout<<"Ingrese el nombre del medicamento: ";getline(cin,nombre);cout<<endl;
		if (se_encuentra(nombre,LISTA_USUARIO)){
		vector <medicina_usuario> aux=busqueda_por_nombre_usuario(a,nombre);
		reporte_para_usuario(aux,35);}
		else {no_se_encuentra_nombre(nombre);}
		floro();
		menu_busqueda_usuario(LISTA_USUARIO,file2);
		}
void interfaz_nombre(vector<medicina_admin> a)
		{string nombre;
		cout<<"Ingrese el nombre del medicamento: ";getline(cin,nombre);cout<<endl;
		if (se_encuentra(nombre,LISTA_ADMIN)){
		vector <medicina_admin> aux=busqueda_por_nombre_admin(a,nombre);
		}else {no_se_encuentra_nombre(nombre);}
		floro();
		menu_busqueda_admin(LISTA_ADMIN);
		}
void interfaz_codigo(vector<medicina_usuario> a,fstream &file2)
		{int codigo;
		cout<<"Ingrese el codigo del medicamento: ";cin>>codigo;cout<<endl;
		if (se_encuentra(codigo,a)){
		medicina_usuario aux=buscar_por_codigo(codigo,a);
		cabecera_usuario("Codigo","Nombre","Tipo de medicamento","Presentacion","Fracciones","Laboratorio","Precio",35);
		aux.imprimir(35);}
		else {no_se_encuentra_codigo(codigo);}
		floro();
		menu_busqueda_usuario(LISTA_USUARIO,file2);
		}
void interfaz_codigo(vector<medicina_admin> a)
		{int codigo;
		cout<<"Ingrese el codigo del medicamento: ";cin>>codigo;cout<<endl;
		if (se_encuentra(codigo,a)){
		medicina_admin aux=buscar_por_codigo(codigo,a);
		cabecera_admin("Codigo","Nombre","Concentracion","Nombre formal","Tipo de medicamento","Presentacion","Fracciones","Fecha","Registro Sanitario","Laboratorio","Precio",35);
		aux.imprimir(35);}
		else {no_se_encuentra_codigo(codigo);}
		floro();
		menu_busqueda_admin(LISTA_ADMIN);
		}
void interfaz_tipo(vector<medicina_usuario> a, fstream &file2)
		{string nombre;
		cout<<"Ingrese el tipo de medicamento: ";getline(cin,nombre);cout<<endl;
		if(se_encuentra_tipo(nombre,LISTA_USUARIO)){
		vector <medicina_usuario> aux=busqueda_por_tipo_usuario(a,nombre);
		reporte_para_usuario(aux,35);}//Hacer que vuelva a menu_busqueda y al principal
		else{no_se_encuentra_tipo(nombre);}
		floro();
		menu_busqueda_usuario(LISTA_USUARIO,file2);
		}
void interfaz_tipo(vector<medicina_admin> a)
		{string nombre;
		cout<<"Ingrese el tipo de medicamento: ";getline(cin,nombre);cout<<endl;
		if(se_encuentra_tipo(nombre,LISTA_ADMIN)){
		vector <medicina_admin> aux=busqueda_por_tipo_admin(a,nombre);
		reporte_para_admin(aux,35);}//Hacer que vuelva a menu_busqueda y al principal
		else{no_se_encuentra_tipo(nombre);}
		floro();
		menu_busqueda_admin(LISTA_ADMIN);
		}
void tab_boleta(string palabra,float x, int espacios)
	{cout.setf(ios::left);

					cout.width(espacios);
					cout<<palabra;
					cout.width(espacios);
					cout<<" ";
					cout.width(espacios);
					cout<<" ";
					cout.width(espacios);
					cout<<" ";
					cout.width(espacios);
					cout.precision(3);
					cout<<x;
					cout<<endl;
	}
void tab_boleta(string palabra,string palabra2, int espacios)
	{cout.setf(ios::left);

					cout.width(espacios);
					cout<<palabra;
					cout.width(espacios);
					cout<<" ";
					cout.width(espacios);
					cout<<" ";
					cout.width(espacios);
					cout<<" ";
					cout.width(espacios);
					cout<<palabra2;
					cout<<endl;
	}
void cabecera_compra(string palabra,string palabra2,string palabra3,string palabra4,string palabra5, int espacios)
	{cout.setf(ios::left);
					cout.width(espacios);
					cout<<palabra;
					cout.width(espacios);
					cout<<palabra2;
					cout.width(espacios);
					cout<<palabra3;
					cout.width(espacios);
					cout<<palabra4;
					cout.width(espacios);
					cout<<palabra5;
					cout<<endl;
	}
void cabecera_usuario(string palabra,string palabra2,string palabra3,string palabra4,string palabra5,string palabra6,string palabra7, int espacios)
	{cout.setf(ios::left);
					cout.width(espacios);
					cout<<palabra;
					cout.width(espacios);
					cout<<palabra2;
					cout.width(espacios);
					cout<<palabra3;
					cout.width(espacios);
					cout<<palabra4;
					cout.width(espacios);
					cout<<palabra5;
					cout.width(espacios);
					cout<<palabra6;
					cout.width(espacios);
					cout<<palabra7;
					cout<<endl;
	}
void cabecera_admin(string palabra,string palabra2,string palabra3,string palabra4,string palabra5,string palabra6,string palabra7,
					string palabra8,string palabra9,string palabra10,string palabra11,int espacios)
					{cout.setf(ios::left);
					cout.width(espacios);
					cout<<palabra;
					cout.width(espacios);
					cout<<palabra2;
					cout.width(espacios);
					cout<<palabra3;
					cout.width(espacios);
					cout<<palabra4;
					cout.width(espacios);
					cout<<palabra5;
					cout.width(espacios);
					cout<<palabra6;
					cout.width(espacios);
					cout<<palabra7;
					cout.width(espacios);
					cout<<palabra8;
					cout.width(espacios);
					cout<<palabra9;
					cout.width(espacios);
					cout<<palabra10;
					cout.width(espacios);
					cout<<palabra11;
					cout<<endl;
					}
void menu_principal_usuario(fstream &file2);
void menu_boleta(vector <medicina_comprada> lista,fstream &file2)
	{
	usuarios user;
	string aux;
	getline(file2,aux);
	user.inicializar(recoge_datos(aux));
	vector <medicina_comprada>::iterator v =lista.begin();
	aux="Cliente: ";
	aux+=user.nombre+"  "+user.apellidoP+"  "+user.apellidoM;
	tab_boleta("Farmacia \"CON FE\"",aux,35);
	aux="DNI: ";
	stringstream aux2;
	aux2<<user.DNI;
		aux+=aux2.str();
	tab_boleta("RUC N° 20468824973",aux,35);
	reporte_para_usuario_compra(lista,35);
	float suma=0,IGV=0.18;
	while (v!=lista.end())
		{suma+=(*v).sub_total;
		file2<<(*v).escribeArchivo(';');
		v++;}
	file2<<endl;
	tab_boleta("IGV (18%)",suma*IGV,35);
	tab_boleta("TOTAL",suma*IGV+suma,35);
	floro();
	file2.close();
	exit(0);
	}
void menu_elimina(vector <medicina_comprada> &lista,fstream &file2)
	{int codigo;
	reporte_para_usuario_compra(lista,35);
	cout<<"Ingrese el codigo del producto a eliminar: ";cin>>codigo;cin.ignore();
	cout<<endl;
	if (se_encuentra(codigo,lista))
		{
		medicina_comprada aux=buscar_por_codigo(codigo,lista);elimina_medicina(lista,codigo);cout<<"Se ha eliminado correctamente: "<<endl;
		aux.imprime(35);
		}
	else {cout<<"No se encuentra el medicamento en la lista de compras";}
	cout<<endl;
	floro();
	menu_post_compra(lista, file2);
	}
void compra_medicina(vector<medicina_usuario> a,fstream &file2);
void compra_medicina2(vector<medicina_usuario> a,vector <medicina_comprada> &lista,fstream &file2);
void menu_reporte_usuario(vector <medicina_usuario> b,fstream &file2);
void menu_reporte_usuario2(vector <medicina_usuario> b, vector <medicina_comprada> &lista,fstream &file2);
void menu_post_compra(vector <medicina_comprada>&b,fstream &file2)//pasa por referencia
	{int opcion;
	cout<<"1. Comprar mas medicina"<<endl;
	cout<<"2. Finalizar compra"<<endl;
	cout<<"3. Eliminar productos del carrito"<<endl;
	cout<<"4. Ver las compras"<<endl;
	cout<<"5. Ver lista de medicamentos"<<endl;//leer opcion
	cout<<"Elija una opcion -> ";cin>>opcion;cin.ignore();
	switch (opcion)
				{
			case 1:	compra_medicina2(LISTA_USUARIO,b,file2);	break;
			case 2: menu_boleta(b,file2);break;//Modulo Boleta
			case 3: menu_elimina(b,file2);break;
			case 4: ver_carrito(b,file2);break;
			case 5: menu_reporte_usuario2(LISTA_USUARIO,b,file2);break;
			default : cout<<"Opcion no valida";menu_post_compra(b,file2);break;
				}

	}
void compra_medicina2(vector<medicina_usuario> a,vector <medicina_comprada> &lista,fstream &file2)
		{int codigo;
		cout<<"Ingrese codigo de medicamento a comprar: ";cin>>codigo;cin.ignore();
		if(se_encuentra(codigo,a)){
		medicina_usuario aux=buscar_por_codigo(codigo,a);
		cabecera_usuario("Codigo","Nombre","Tipo de medicamento","Presentacion","Fracciones","Laboratorio","Precio",35);
		aux.imprimir(35);
		char resp;
			cout<<"Comprar? [S/N] "<<endl;
			cout<<"-> ";
			do{
			cin>>resp;cin.ignore();
			resp=toupper(resp);} while (resp!='S'&&resp!='N');
		if(resp=='S')
			{
				int cantidad;
				cout<<endl<<"Cuantos?";
				do {cin>>cantidad;cin.ignore();} while (cantidad<1);
				agrega_medicina_comprada(aux,cantidad,lista);
				cout<<"Registro exitoso"<<endl;
				cout<<"Continuar comprando?"<<endl;
				char resp2;
				do{
							cin>>resp2;cin.ignore();
							resp2=toupper(resp2);} while (resp2!='S'&&resp2!='N');
				if (resp2=='S')
					{menu_post_compra(lista,file2);}
				else {menu_boleta(lista,file2);}
			}
		else {menu_post_compra(lista,file2);}
								}
		else {no_se_encuentra_codigo(codigo);floro();menu_post_compra(lista,file2);}
		}
void compra_medicina(vector<medicina_usuario> a,fstream &file2)
		{int codigo;
		cout<<"Ingrese codigo de medicamento a comprar: ";cin>>codigo;cin.ignore();
		if(se_encuentra(codigo,LISTA_USUARIO)){
		medicina_usuario aux=buscar_por_codigo(codigo,a);
		vector <medicina_comprada> lista_de_compras;
		cabecera_usuario("Codigo","Nombre","Tipo de medicamento","Presentacion","Fracciones","Laboratorio","Precio",35);
		aux.imprimir(35);
		char resp;
			cout<<"Comprar? [S/N] "<<endl;
			cout<<"-> ";
			do{
			resp=cin.get();cin.ignore();
			resp=toupper(resp);} while ((resp!='S') && (resp!='N'));
		if(resp=='S')
			{
				int cantidad;
				cout<<endl<<"Cuantos?";
				do {cin>>cantidad;cin.ignore();} while (cantidad<1);
				agrega_medicina_comprada(aux,cantidad,lista_de_compras);
				cout<<"Registro exitoso"<<endl;
				cout<<"Continuar comprando?"<<endl;
				char resp2;
				do{
							cin>>resp2;cin.ignore();
							resp2=toupper(resp2);} while (resp2!='S'&&resp2!='N');
				if (resp2=='S')
					{menu_post_compra(lista_de_compras,file2);}
				else {menu_boleta(lista_de_compras,file2);}
			}
		else {menu_principal_usuario(file2);}
											}
		else {no_se_encuentra_codigo(codigo);floro();menu_principal_usuario(file2);}
		}

void menu_busqueda_usuario(vector <medicina_usuario> b, fstream &file2)
	{
	cout<<"1. Buscar por nombre"<<endl;
	cout<<"2. Buscar por tipo de medicamento"<<endl;
	cout<<"3. Buscar por codigo"<<endl;
	cout<<"4. Volver"<<endl;
	int opcion;
	cout<<"Elija una opcion -> ";cin>>opcion;cin.ignore();
		switch (opcion)
			{
		case 1:	interfaz_nombre(b,file2);	break;
		case 2: interfaz_tipo(b,file2);break;
		case 3: interfaz_codigo(b,file2);break;
		case 4: menu_principal_usuario(file2);break;
		default : cout<<"Opcion no valida";menu_busqueda_usuario(b,file2);break;
			}
	}
void menu_reporte_usuario2(vector <medicina_usuario> b, vector <medicina_comprada> &lista, fstream &file2)
	{
	cout<<"1. Ordenar por nombre"<<endl;
	cout<<"2. Ordenar por tipo de medicamento"<<endl;
	cout<<"3. Ordenar por codigo"<<endl;
	cout<<"4. Volver"<<endl;
	int opcion;
	cout<<"Elija una opcion -> ";cin>>opcion;cin.ignore();
		switch (opcion)//Verificar que sea un numero
			{
		case 1:	ordena_lista_usuario_nombre_desc(b);reporte_para_usuario(b,35);break;
		case 2: ordena_lista_usuario_tipo_desc(b);reporte_para_usuario(b,35);break;//agregar opcion de ordenar ascendentemente
		case 3: ordena_lista_usuario_codigo_desc(b);reporte_para_usuario(b,35);break;
		case 4: menu_post_compra(lista,file2);break;
		default : cout<<"Opcion no valida";menu_reporte_usuario(b,file2);break;
			}
	}
void menu_reporte_usuario(vector <medicina_usuario> b,fstream &file2)
	{
	cout<<"1. Ordenar por nombre"<<endl;
	cout<<"2. Ordenar por tipo de medicamento"<<endl;
	cout<<"3. Ordenar por codigo"<<endl;
	cout<<"4. Volver"<<endl;
	int opcion;
	cout<<"Elija una opcion -> ";cin>>opcion;cin.ignore();
		switch (opcion)//Verificar que sea un numero
			{
		case 1:	ordena_lista_usuario_nombre_desc(b);reporte_para_usuario(b,35);break;
		case 2: ordena_lista_usuario_tipo_desc(b);reporte_para_usuario(b,35);break;//agregar opcion de ordenar ascendentemente
		case 3: ordena_lista_usuario_codigo_desc(b);reporte_para_usuario(b,35);break;
		case 4: menu_principal_usuario(file2);break;
		default : cout<<"Opcion no valida";menu_reporte_usuario(b,file2);break;
			}
	}
void menu_principal_usuario(fstream &file2 )
	{
	cout.width(35);
	cout<<" ";
	cout<<"MENU PARA USUARIOS"<<endl;
	cout<<"---------------------------------------------------------------------"<<endl;
	cout<<"1. Mostrar lista de medicamentos"<<endl;
	cout<<"2. Buscar medicamentos"<<endl;
	cout<<"3. Compra de medicamentos"<<endl;
	cout<<"4. Salir"<<endl;
	int opcion;
	cout<<"Elija una opcion -> ";cin>>opcion;cin.ignore();
		switch (opcion)
			{
		case 1:	{menu_reporte_usuario(LISTA_USUARIO,file2)
				;
				break;}
		case 2: menu_busqueda_usuario(LISTA_USUARIO,file2);break;
		case 3: compra_medicina(LISTA_USUARIO,file2);break;
		case 4: break;
		default : cout<<"Opcion no valida"<<endl;menu_principal_usuario(file2);break;
			}
	}



void menu_principal_admin();

void menu_reporte_admin(vector <medicina_admin> b);
void menu_busqueda_admin(vector <medicina_admin> b)
	{
	cout<<"1. Buscar por nombre"<<endl;
	cout<<"2. Buscar por tipo de medicamento"<<endl;
	cout<<"3. Buscar por codigo"<<endl;
	cout<<"4. Volver"<<endl;
	int opcion;
	cout<<"Elija una opcion -> ";cin>>opcion;cin.ignore();
		switch (opcion)
			{
		case 1:	interfaz_nombre(b);	break;
		case 2: interfaz_tipo(b);break;
		case 3: interfaz_codigo(b);break;
		case 4: menu_principal_admin();break;
		default : cout<<"Opcion no valida";menu_busqueda_admin(b);break;
			}
	}
void menu_reporte_admin2(vector <medicina_admin> b, vector <medicina_comprada> &lista)
	{
	cout<<"1. Ordenar por nombre"<<endl;
	cout<<"2. Ordenar por tipo de medicamento"<<endl;
	cout<<"3. Ordenar por codigo"<<endl;
	cout<<"4. Volver"<<endl;
	int opcion;
	cout<<"Elija una opcion -> ";cin>>opcion;cin.ignore();
		switch (opcion)//Verificar que sea un numero
			{
		case 1:	ordena_lista_admin_nombre_desc(b);reporte_para_admin(b,35);break;
		case 2: ordena_lista_admin_tipo_desc(b);reporte_para_admin(b,35);break;//agregar opcion de ordenar ascendentemente
		case 3: ordena_lista_admin_codigo_desc(b);reporte_para_admin(b,35);break;
		default : cout<<"Opcion no valida";menu_reporte_admin(b);break;
			}
	}
void menu_reporte_admin(vector <medicina_admin> b)
	{
	cout<<"1. Ordenar por nombre"<<endl;
	cout<<"2. Ordenar por tipo de medicamento"<<endl;
	cout<<"3. Ordenar por codigo"<<endl;
	cout<<"4. Volver"<<endl;
	int opcion;
	cout<<"Elija una opcion -> ";cin>>opcion;cin.ignore();
		switch (opcion)//Verificar que sea un numero
			{
		case 1:	ordena_lista_admin_nombre_desc(b);reporte_para_admin(b,35);break;
		case 2: ordena_lista_admin_tipo_desc(b);reporte_para_admin(b,35);break;//agregar opcion de ordenar ascendentemente
		case 3: ordena_lista_admin_codigo_desc(b);reporte_para_admin(b,35);break;
		case 4: menu_principal_admin();break;
		default : cout<<"Opcion no valida";menu_reporte_admin(b);break;
			}
	}
void menu_principal_admin()
	{
	cout<<"1. Mostrar lista de medicamentos"<<endl;
	cout<<"2. Buscar medicamentos"<<endl;
	cout<<"3. Salir"<<endl;
	int opcion;
	cout<<"Elija una opcion -> ";cin>>opcion;cin.ignore();
		switch (opcion)
			{
		case 1:	{menu_reporte_admin(LISTA_ADMIN)
				;
				break;}
		case 2: menu_busqueda_admin(LISTA_ADMIN);break;
		case 3: break;
		default : cout<<"Opcion no valida"<<endl;menu_principal_admin();break;
			}
	}


void pedirdatos(){
	usuarios nuevo;
	string aux;
	fflush(stdin);
	cout<<"Escriba su Nombre de Usuario: " ;
	getline(cin,nuevo.nombre);
	ApellidosyNombres(nuevo.nombre);
	cout<<endl;
	fflush(stdin);
	cout<<"Ingrese su apellido paterno: ";
	getline(cin,nuevo.apellidoP);
	ApellidosyNombres(nuevo.apellidoP);
	cout<<endl;
	fflush(stdin);
	cout<<"Ingrese su apellido materno: ";
		getline(cin,nuevo.apellidoM);
		ApellidosyNombres(nuevo.apellidoM);
		cout<<endl;
		fflush(stdin);
	cout<<"Ingrese su edad: ";//Seria mejor con fecha ya que la edad cambia, pero bueno...
	cin>>nuevo.edad;
	cin.ignore();
	cout<<endl;
	fflush(stdin);
	cout<<"Ingrese su DNI: ";
	cin>>nuevo.DNI;
	cin.ignore();
	cout<<endl;
	fflush(stdin);
	cout<<"Escriba su contrasenia: ";
	getline(cin,nuevo.contra);
	cout<<endl;
	fflush(stdin);
	string filename = "datos/personas/usuarios/"+nuevo.apellidoP+nuevo.apellidoM+nuevo.nombre+".txt";
	ofstream file (filename.c_str(),ios::out);
	file<<nuevo.escribeArchivo(';');
	file<<endl;
	file.close();
	fstream file2 (filename.c_str());
	bienvenidousuario();
	menu_principal_usuario(file2);
	}
void clean(){
	system("CLS");
}


void fin(){
	cout<<"El Programa se cerrara 3 segundos";
	Sleep(1000);
	cout<<", 2 segundos, ";
	Sleep(1000);
	cout<<"1 segundo...";
	exit(0);
}


int ExisteArchivo(const char* filename) {
	FILE* f = NULL;
	f = fopen(filename,"r");
	if (f == NULL and errno == ENOENT)
		return 0;
	else {
		fclose(f);
		return 1;
	}
}

void bienvenidousuario(){
	Sleep(2000);
	cout<<endl;
	cout<<"Bienvenido Usuario"<<endl;
	Sleep(2000);
}

void bienvenidoadministrador(){
	Sleep(2000);
	cout<<endl;
	cout<<"Bienvenido Administrador"<<endl;
}

struct maxim
	{int tipo;
	int concentracion;
	};
bool se_encuentra (string nombre,vector <string> b)
	{vector<string>::iterator v=b.begin();
	bool resp=false;
	while (v!=b.end())
		{if (nombre==(*v)){resp=true;break;}
		v++;
		}
	return resp;
	}
bool se_encuentra_2 (int concentracion,vector <int> b)
	{vector<int>::iterator v=b.begin();
	bool resp=false;
	while (v!=b.end())
		{if (concentracion==(*v)){resp=true;break;}
		v++;
		}
	return resp;
	}
maxim max_num (vector <medicina_usuario> lista)		//Funcion utilitaria para encontrar el max numero de tipo y concentracion en el archivo
	{maxim aux;
		vector <medicina_usuario>::iterator v= lista.begin();
		vector<string> compara1;

		vector<int> compara2;
		compara1.push_back((*v).nom_simpl);
		compara2.push_back((*v).fracciones);
		v++;

		while (v!=lista.end())
			{
				if (!se_encuentra((*v).nom_simpl,compara1)){compara1.push_back((*v).nom_simpl);}
				if (!se_encuentra_2((*v).fracciones,compara2)){compara2.push_back((*v).fracciones);}
				v++;
			}
		int tipo=compara1.size();
		int concen=compara2.size();
		aux.tipo=tipo;
		aux.concentracion=concen;
		return aux;
	}
vector <string> recoge_tipos(vector <medicina_usuario> lista)
		{ vector<medicina_usuario>::iterator v=lista.begin();
			vector <string> aux;
			aux.push_back((*v).nom_simpl);
			v++;
			while (v!=lista.end())
				{
					if (!se_encuentra(v->nom_simpl,aux)){aux.push_back((*v).nom_simpl);}
					v++;
				}
			return aux;
		}
vector <int> recoge_concentracion(vector <medicina_usuario> lista)
		{ vector<medicina_usuario>::iterator v=lista.begin();
			vector <int> aux;
			aux.push_back((*v).fracciones);
			v++;
			while (v!=lista.end())
				{
					if (!se_encuentra_2(v->fracciones,aux)){aux.push_back((*v).fracciones);}
					v++;
				}
			return aux;
		}
bool *numero_max_switch(int opciones)//
	{bool *aux=new bool [10];//Asumiendo 10 como max numero de opciones
	for (int i=0;i<10;i++)
		{
			while (i<opciones)
				{*(aux+i)=true;i++;}
			*(aux+i)=false;
		}
	return aux;
	}
void submenu_compra_2(int opciones,vector<medicina_usuario> lista,vector <int> fracciones)//opciones es lista.size()
	{int opcion;
	bool *aux=numero_max_switch(opciones);
	for (int i=0;i<opciones;i++)
		{
			cout<<i<<". "<<fracciones.at(i)<<endl;
		}
	cout<<"Elija una opcion: ";cin>>opcion;

	}
void submenu_opciones2 (int opciones,int opcion, vector <medicina_usuario> lista, vector <int> tipos,bool aux[])
		{opcion-=1;
	switch (opcion)
		{case 1: {vector <medicina_usuario> sub_lista=busqueda_por_concentracion(lista,tipos.at(opcion));break;}//falta implementar compra
		case 2: if (aux[opcion]){vector <medicina_usuario> sub_lista=busqueda_por_concentracion(lista,tipos.at(opcion));}
					else {submenu_opciones2(opciones,-1,lista,tipos,aux);}
					break;
		case 3:		if (aux[opcion]){vector <medicina_usuario> sub_lista=busqueda_por_concentracion(lista,tipos.at(opcion));}
					else {submenu_opciones2(opciones,-1,lista,tipos,aux);}
					break;
		case 4:		if (aux[opcion]){vector <medicina_usuario> sub_lista=busqueda_por_concentracion(lista,tipos.at(opcion));}
							else {submenu_opciones2(opciones,-1,lista,tipos,aux);}
							break;
		case 5:		if (aux[opcion]){vector <medicina_usuario> sub_lista=busqueda_por_concentracion(lista,tipos.at(opcion));}
							else {submenu_opciones2(opciones,-1,lista,tipos,aux);}
							break;
		case 6:		if (aux[opcion]){vector <medicina_usuario> sub_lista=busqueda_por_concentracion(lista,tipos.at(opcion));}
							else {submenu_opciones2(opciones,-1,lista,tipos,aux);}
							break;
		case 7:		if (aux[opcion]){vector <medicina_usuario> sub_lista=busqueda_por_concentracion(lista,tipos.at(opcion));}
							else {submenu_opciones2(opciones,-1,lista,tipos,aux);}
							break;
		case 8:		if (aux[opcion]){vector <medicina_usuario> sub_lista=busqueda_por_concentracion(lista,tipos.at(opcion));}
							else {submenu_opciones2(opciones,-1,lista,tipos,aux);}
							break;
		case 9:		if (aux[opcion]){vector <medicina_usuario> sub_lista=busqueda_por_concentracion(lista,tipos.at(opcion));}
							else {submenu_opciones2(opciones,-1,lista,tipos,aux);}
							break;
		default: cout<<"Opcion no encontrada..."<<endl;floro();submenu_compra_2(opciones,lista,tipos);

			}
		}
void submenu_compra_1(int opciones,vector<medicina_usuario> lista,vector <string> tipos)
	{int opcion;
	bool *aux=numero_max_switch(opciones);
	for (int i=0;i<opciones;i++)
		{
			cout<<i<<". "<<tipos.at(i)<<endl;
		}
	cout<<"Elija una opcion: ";cin>>opcion;

	}
void submenu_opciones1 (int opciones,int opcion, vector <medicina_usuario> lista, vector <string> tipos,bool aux[])
		{
	opcion-=1;
	switch (opcion)
		{case 1: {vector <medicina_usuario> sub_lista=busqueda_por_tipo_usuario(lista,tipos.at(opcion));break;}//falta implementar el submenu2
		case 2: if (aux[opcion]){vector <medicina_usuario> sub_lista=busqueda_por_tipo_usuario(lista,tipos.at(opcion));
					vector <int> aux2=recoge_concentracion(sub_lista);submenu_compra_2(aux2.size(),sub_lista,aux2);}
					else {submenu_opciones1(opciones,-1,lista,tipos,aux);}
					break;
		case 3:		if (aux[opcion]){
										vector <medicina_usuario> sub_lista=busqueda_por_tipo_usuario(lista,tipos.at(opcion));
										vector <int> aux2=recoge_concentracion(sub_lista);submenu_compra_2(aux2.size(),sub_lista,aux2);
									}
					else {submenu_opciones1(opciones,-1,lista,tipos,aux);}
					break;
		case 4:		if (aux[opcion]){vector <medicina_usuario> sub_lista=busqueda_por_tipo_usuario(lista,tipos.at(opcion));
									vector <int> aux2=recoge_concentracion(sub_lista);submenu_compra_2(aux2.size(),sub_lista,aux2);}
							else {submenu_opciones1(opciones,-1,lista,tipos,aux);}
							break;
		case 5:		if (aux[opcion]){vector <medicina_usuario> sub_lista=busqueda_por_tipo_usuario(lista,tipos.at(opcion));
									vector <int> aux2=recoge_concentracion(sub_lista);submenu_compra_2(aux2.size(),sub_lista,aux2);}
							else {submenu_opciones1(opciones,-1,lista,tipos,aux);}
							break;
		case 6:		if (aux[opcion]){vector <medicina_usuario> sub_lista=busqueda_por_tipo_usuario(lista,tipos.at(opcion));
										vector <int> aux2=recoge_concentracion(sub_lista);submenu_compra_2(aux2.size(),sub_lista,aux2);}
							else {submenu_opciones1(opciones,-1,lista,tipos,aux);}
							break;
		case 7:		if (aux[opcion]){vector <medicina_usuario> sub_lista=busqueda_por_tipo_usuario(lista,tipos.at(opcion));
									vector <int> aux2=recoge_concentracion(sub_lista);submenu_compra_2(aux2.size(),sub_lista,aux2);}
							else {submenu_opciones1(opciones,-1,lista,tipos,aux);}
							break;
		case 8:		if (aux[opcion]){vector <medicina_usuario> sub_lista=busqueda_por_tipo_usuario(lista,tipos.at(opcion));
									vector <int> aux2=recoge_concentracion(sub_lista);submenu_compra_2(aux2.size(),sub_lista,aux2);}
							else {submenu_opciones1(opciones,-1,lista,tipos,aux);}
							break;
		case 9:		if (aux[opcion]){vector <medicina_usuario> sub_lista=busqueda_por_tipo_usuario(lista,tipos.at(opcion));
										vector <int> aux2=recoge_concentracion(sub_lista);submenu_compra_2(aux2.size(),sub_lista,aux2);}
							else {submenu_opciones1(opciones,-1,lista,tipos,aux);}
							break;
		default: cout<<"Opcion no encontrada..."<<endl;floro();submenu_compra_1(opciones,lista,tipos);

			}
		}
maxim max=max_num(LISTA_USUARIO);
const int MAXIMO_TIPO_DE_MEDICAMENTOS= 31;//El valor es 31
const int MAXIMO_CONCENTRACION= 75;//El valor es 75

struct admin
	{
		string nombre;
		string contrasenia;
		void llenaDatos(vector <string> linea)
			{
				vector <string>::iterator v=linea.begin();
				nombre=(*v);v++;
				contrasenia=(*v);
			}

	};
vector <admin> listaAdministradores()
				{
	ifstream file ("datos/personas/admin.txt",ios::in);
		string aux;
		vector <admin> administradores;
		admin tmp;
		while (getline(file,aux))
			{
				tmp.llenaDatos(recoge_datos(aux));
				administradores.push_back(tmp);
			}
		return administradores;
				}
bool seEncuentra(vector <admin> lista, admin alguien)
	{bool resp=false;
		vector <admin>::iterator v=lista.begin();
		while (v!=lista.end())
			{
				if ((*v).nombre==alguien.nombre&&(*v).contrasenia==alguien.contrasenia)
					{resp=true;break;}
				v++;
			}
		return resp;
	}
void administradores(){
	vector <admin> lista=listaAdministradores();
	admin input;
	cout<<"Credenciales"<<endl;
	cout<<"Ingrese su nombre de administrador: ";
	string aux;
	getline (cin,aux);
	input.nombre=aux;
	cout<<endl<<"Ingrese su contrasenya: ";
	getline(cin,aux);
	input.contrasenia=aux;
	if (seEncuentra(lista,input))
		{
			menu_principal_admin();
		}
	else
		{cout<<"Nombre y/o contrasenya incorrecta";}
}
void ApellidosyNombres(string &linea)
	{
		quita_espacios(linea);//Falta tomar en cuenta apellidos como De la Cruz
		linea[0]=toupper(linea.c_str()[0]);
		for (unsigned int i=1;i<linea.length();i++)
			{linea[i]=tolower(linea.c_str()[i]);}
	}
void login_usuario()
	{	string filename;
		string aux;
		cout<<"Ingrese su nombre: ";
		getline (cin,aux);
		ApellidosyNombres(aux);
		filename=aux;
		cout<<"Ingrese su Apellido Paterno: ";
				getline (cin,aux);
				ApellidosyNombres(aux);
				filename.insert(0,aux);
				int n=aux.length();
				cout<<"Ingrese su Apellido Materno: ";
						getline (cin,aux);
						ApellidosyNombres(aux);
						filename.insert(n,aux);
						filename.insert(0,"datos/personas/usuarios/");
						filename+=".txt";
						int valida=ExisteArchivo(filename.c_str());
													if(valida==0){
														cout<<"Ese usuario no existe."<<endl;
													}
													else{
														ifstream file (filename.c_str(),ios::in);
														usuarios user;

														getline(file,aux);
														user.inicializar(recoge_datos(aux));
														cout<<"		Contrasenia: ";
														string con;
														getline(cin,con);
														if(user.contra==con){
															bienvenidousuario();
															fstream file2 (filename.c_str());
															menu_principal_usuario(file2);
															file2.close();
														}
														else{
															cout<<"			Contrasenia incorrecta"<<endl<<endl;
															cout<<"			Intentelo de nuevo."<<endl;
															cout<<"			Contrasenia: ";
															cin>>con;
															if(user.contra==con){
																bienvenidousuario();
																fstream file2 (filename.c_str());
																menu_principal_usuario(file2);
																file2.close();
															}
															else{
																cout<<"			Contrasenia incorrecta, el programa volvera al menu principal en 2 segundos, ";
																Sleep(1000);
																cout<<"1 segundo...";
																clean();

															}
														}
														file.close();


													}

	}
void menuInicial()
	{
	int menu;
	cout<<"\n\n\n\n"<<endl ;
    cout<<"\t �����������������������������������������������������ͻ "<<endl;
    cout<<"\t � ";
    cout.width(55);
    cout<<right<<"Bienvenido a la farmacia virtual         �  "<< endl ;
    cout<<"\t � --------------------------------------------------- � "<< endl ;
    cout<<"\t �����������������������������������������������������ͼ "<<endl<<endl;
	Sleep(2000);
	clean();
	cout.width(30);
	cout<<" ";
	cout.width(30);
	cout<<left<<"MODULO PRINCIPAL"<<endl;
	cout<<left<<"BIENVENIDO"<<endl;
	cout << "----------------------------" << endl;
	cout << "1. Ingrese como administrador"<<endl;
	cout << "2. Ingresar como Usuario" << endl;
	cout << "3. Registrarse como Usuario" << endl;
	cout << "4. Salir del Programa." << endl;
	cout << "--------------------------"<<endl;
	cout <<"Escoja una opcion: ";
	cin>>menu;
	cin.ignore();
	if (menu==4){
		fin();
	}
	switch(menu){
		case 1:{
				administradores(); break;
				};
		case 2:{login_usuario();menuInicial();break;}
		case 3:{
					pedirdatos();

					menuInicial();
					break;
				};
				}
	}
int main()
	{
	menuInicial();
			}
