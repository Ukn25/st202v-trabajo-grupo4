/*
 * funciones.h
 *
 *  Created on: 30 nov. 2017
 *      Author: Grupo4
 */

#ifndef F1_H_
#define F1_H_
#include <iostream>
#include <fstream>
#include <windows.h>
#include <conio.h>
#include <string.h>
#include <cstring>
#include <cstdlib>
#include <dos.h>
#include <ctype.h>
#include <iomanip>
#include <vector>
using namespace std;

void pedirdatos();
void clean();
void usuario();
void fin();
int ExisteArchivo(char* filename);
void bienvenidoadministrador();
void bienvenidousuario();//pendiente
void administradores();



#endif /* F1_H_ */
